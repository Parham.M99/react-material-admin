import { createTheme} from '@mui/material/styles';


const theme = createTheme({
    direction: 'rtl',
    typography:{
        fontFamily:[
            "Shabnam"
        ]
    }
})


export default theme;