import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Sidebar from "../components/controls/Sidebar";
import Header from "../components/controls/Header";
  import CssBaseline from "@mui/material/CssBaseline";
import { StyleSheetManager } from 'styled-components';
import rtlPlugin from 'stylis-plugin-rtl';
import PageHeader from "../components/controls/PageHeader";
import Employee from "../components/pages/Employees/Employee";
function App() {
    return (
        <>
        
            <Grid container> 
                 <Grid item xs={2} >
                    <Sidebar />
                </Grid>
                <Grid item xs={10}>
                    <Header />  
                    <PageHeader/>
                    <Employee/>
                 </Grid>
            </Grid>
        </>
    );
}

export default App;
