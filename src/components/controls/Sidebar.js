import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { styled } from '@mui/material/styles';

const BoxContainer = styled(Box)(({theme})=>({
    background: "#253053",
    minHeight:"100vh",
    width:"100%",
    padding:"0",
    margin:"0",
}))



function Sidebar() {
    return (
        <BoxContainer >
        </BoxContainer>
    );
}

export default Sidebar;
