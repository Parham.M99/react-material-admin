import Paper from "@mui/material/Paper";
import GroupIcon from "@mui/icons-material/Group";
import Card from "@mui/material/Card";
import { styled, alpha } from "@mui/material/styles";
import { Typography } from "@mui/material";
import PeopleOutlineRoundedIcon from "@mui/icons-material/PeopleOutlineRounded";
import { display } from "@mui/system";
const PageContainer = styled("div")(({ theme }) => ({
    display: "flex",
    userSelect:"none",
    marginRight: theme.spacing(5),
    marginBottom:theme.spacing(2),
    marginTop:theme.spacing(-2),
    [theme.breakpoints.down("sm")]:{
        
        marginRight: theme.spacing(2),
    }
}));

const CardStyle = styled(Card)(({ theme }) => ({
    padding: theme.spacing(2),
    [theme.breakpoints.down("sm")]:{
        
        // padding: theme.spacing(0),
        paddingBottom: theme.spacing(-2)
    }
}));

const PageTitle = styled("div")(({theme})=>({
    marginRight: "1.5rem",
    marginTop:theme.spacing(1),
    [theme.breakpoints.down("sm")]:{
        marginRight: "1rem",

    }
}))

const TypographyTitle = styled(Typography)(({ theme })=>({
    fontSize: "23px",
    fontFamily:"Shabnam-bold",
    [theme.breakpoints.down("sm")]:{
        fontSize: "20px",
    }
}))


const TypographyCaption = styled(Typography)(({ theme })=>({
    fontFamily:"Shabnam-light",
    display: "block",
    opacity:".7",
    [theme.breakpoints.down("sm")]:{
        fontSize: "16px",
    }

}))

function PageHeader() {
    return (
        <Paper elevation={0} square sx={{ marginTop: 5 }}>
            <PageContainer>
                <CardStyle>
                    <PeopleOutlineRoundedIcon
                        sx={{ fontSize: {sm:"3.5rem",xs:"2.5rem" } , color:"#3c44b1"}}
                    />
                </CardStyle>
                <PageTitle>
                    <TypographyTitle variant="h6" component="div" >
                        کارمندان جدید
                    </TypographyTitle>
                    <TypographyCaption variant="h6" component="div">
                        فرم طراحی شده با اعتبارسنجی
                    </TypographyCaption>
                </PageTitle>
            </PageContainer>
        </Paper>
    );
}

export default PageHeader;
