import { MenuItem, TextField } from "@mui/material";
import { useField, useFormikContext } from "formik";
import { styled } from "@mui/material/styles";

function SelectWrapper({ name, options, ...otherProps }) {
    const [field, meta] = useField(name);

    const { setFieldValue, values } = useFormikContext();

    const handleChange = (evt) => {
        const { value } = evt.target;
        setFieldValue(name, value);
    };

    const selectConfig = {
        ...otherProps,
        ...field,
        fullWidth: true,
        variant: "standard",
        select: true,
        onChange: handleChange,
    };

    if (meta && meta.touched && meta.error) {
        selectConfig.error = true;
        selectConfig.helperText = meta.error;
    }

    const TextFieldSelect = styled(TextField)(({ theme })=>({
        marginTop:".7em",
        "& label": {
            transformOrigin: "top right",
            right: 0,
            left: "auto",
        },
        ".muirtl-pqjvzy-MuiSvgIcon-root-MuiSelect-icon":{
            left: 0,
            right: "auto",
        }
    }))

    return (
        <TextFieldSelect {...selectConfig}>
            {Object.keys(options).map((item, pos) => {
                return (
                    <MenuItem sx={{direction:"rtl"}} key={pos} value={item}>
                        {options[item]}
                    </MenuItem>
                );
            })}
        </TextFieldSelect>
    );
}

export default SelectWrapper;
