import { TextField } from "@mui/material";
import { useField } from "formik";
import { styled } from "@mui/material/styles";


const TextFieldSx = styled(TextField)(({theme }) => ({
    marginTop:".7em",
    "& label": {
        transformOrigin: "top right",
        right: 0,
        left: "auto",
    },
}))


function TextFieldWrapper({ name, ...otherProps }) {
    const [field, meta] = useField(name);

    const textFieldConfig = {
        ...field,
        ...otherProps,
        fullWidth: true,
        id:"standard-basic",
        variant:"standard"
    };

    if (meta && meta.touched && meta.error) {
        textFieldConfig.error = true;
        textFieldConfig.helperText = meta.error;
    }

    return <TextFieldSx {...textFieldConfig} />;
}

export default TextFieldWrapper;
