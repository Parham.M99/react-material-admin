import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import { useField, useFormikContext } from "formik";
import { useState } from "react";

function DatePickerWrapper({ name, ...otherProps }) {
    const [field, meta] = useField(name);

    const { setFieldValue } = useFormikContext();

    const [valueDate, setValueDate] = useState();

    const textFieldConfig = {
        ...field,
        fullWidth: true,
    };

    if (meta && meta.touched && meta.error) {
        textFieldConfig.error = true;
        textFieldConfig.helperText = meta.error;
    }

    const style = {
        "& label": {
            transformOrigin: "top right",
            right: 0,
            left: "auto",
        },
        marginTop: ".6rem",
    };

    return (
        <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
                label="تاریخ استخدام"
                value={valueDate}
                onChange={(newValue) => {
                    setValueDate(newValue);
                    setFieldValue(newValue);
                }}
                renderInput={(params) => (
                    <TextField
                {...textFieldConfig}
                        sx={style}
                        id="standard-basic"
                        variant="standard"
                        {...params}
                    />
                )}
            />
        </LocalizationProvider>
    );
}

export default DatePickerWrapper;
