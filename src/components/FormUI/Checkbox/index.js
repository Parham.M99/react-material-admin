import { Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel } from "@mui/material";
import { useField, useFormikContext } from "formik";

function CheckboxWrapper({ name, label, legend, ...otherProps }) {
    const [field, meta] = useField(name);
    const { setFieldValue } = useFormikContext();

    const handleChange = (evt) => {
        const { value } = evt.target;
        setFieldValue(name, value);
    };

    const configCheckbox = {
        ...otherProps,
        ...field,
        fullWidth: true,
        onChange: handleChange,
    };

    const configFormControl = {};
    if (meta && meta.touched && meta.error) {
        configFormControl.error = true;
    }

    

    return (
        <FormControl {...configFormControl} sx={{mt:2}} >
            <FormLabel component="legend">{legend}</FormLabel>
            <FormGroup >
                <FormControlLabel
                    sx={{mr:0}}
                    control={<Checkbox {...configCheckbox} />}
                    label={label}
                />
            </FormGroup>
        </FormControl>
    );
}

export default CheckboxWrapper;
