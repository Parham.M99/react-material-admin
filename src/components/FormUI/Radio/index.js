import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { useField, useFormikContext } from "formik";

function RadioWrapper({ name, legend, ...otherProps }) {
    const [field, meta] = useField(name);
    const { setFieldValue, value } = useFormikContext();

    const handleChange = (evt) => {
        const { value } = evt.target;
        setFieldValue(name, value);
    };

    const radioConfig = {
        ...field,
        ...otherProps,
        onchange: handleChange,
    };
    const configFormControl = {};
    if (meta && meta.touched && meta.error) {
        configFormControl.error = true;
    }
    return (
        <FormControl component="fieldset" {...configFormControl}>
            <FormLabel component="legend">{legend}</FormLabel>
            <RadioGroup
            row
                aria-label="gender"
                defaultValue="male"
                name="radio-buttons-group">
                <FormControlLabel
                    {...radioConfig}
                    value="male"
                    control={<Radio />}
                    label="مرد"
                />
                <FormControlLabel
                    {...radioConfig}
                    onClick={handleChange}
                    value="female"
                    control={<Radio />}
                    label="زن"
                />
            </RadioGroup>
        </FormControl>
    );
}

export default RadioWrapper;
