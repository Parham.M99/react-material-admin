import * as React from "react";
import PropTypes from "prop-types";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import { useSpring, animated } from "react-spring";
import AddIcon from "@mui/icons-material/Add";
import { styled } from "@mui/material/styles";
import IconButton from "@mui/material/IconButton";
import CancelPresentationRoundedIcon from "@mui/icons-material/CancelPresentationRounded";
import Grid from "@mui/material/Grid";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextField from "../../FormUI/TextField";
import Select from "../../FormUI/Select";
import city from "../../../data/countries.json";
import Radio from "../../FormUI/Radio";
import departmentField from "../../../data/departmentField.json";
import DatePicker from "../../FormUI/DatePicker";
import Checkbox from "../../FormUI/Checkbox";
import ButtonWrapper from "../../FormUI/SubmitBtn";
import { Button, Stack } from "@mui/material";

const Fade = React.forwardRef(function Fade(props, ref) {
    const { in: open, children, onEnter, onExited, ...other } = props;
    const style = useSpring({
        from: { opacity: 0 },
        to: { opacity: open ? 1 : 0 },
        onStart: () => {
            if (open && onEnter) {
                onEnter();
            }
        },
        onRest: () => {
            if (!open && onExited) {
                onExited();
            }
        },
    });

    return (
        <animated.div ref={ref} style={style} {...other}>
            {children}
        </animated.div>
    );
});

const INITIAL_FORM_STATE = {
    fullName: "",
    email: "",
    moblieNumber: "",
    city: "",
    department: "",
    hireDate: "",
    termsOfService: false,
};

const FORM_VALIDATION = Yup.object({
    fullName: Yup.string().required("لطفا نام و نام خانوادگی خود را وارد کنید"),
    email: Yup.string()
        .email("لطفا ایمیل خود را به درستی وارد کنید")
        .required("لطفا ایمیل خود را وارد کنید"),
    moblieNumber: Yup.number()
        .integer("لطفا شماره موبایل معتبر وارد کنید")
        .typeError("لطفا شماره موبایل معتبر وارد کنید")
        .required("لطفا شماره موبایل خود را وارد کنید"),
    city: Yup.string().required("لطفا شهر خود را انتخاب کنید"),
    department: Yup.string().required("لطفا واحد سازمانی خود را وارد کنید"),
    hireDate: Yup.date().required("need"),
    termsOfService: Yup.boolean()
        .oneOf([true], "The terms and conditions must be accepted.")
        .required("The terms and conditions must be accepted."),
});

Fade.propTypes = {
    children: PropTypes.element,
    in: PropTypes.bool.isRequired,
    onEnter: PropTypes.func,
    onExited: PropTypes.func,
};

const TypographyTitle = styled(Typography)(({ theme }) => ({}));

const HeaderContainer = styled(Grid)(({ theme }) => ({
    paddingBottom: theme.spacing(2),
    borderBottom: `2px solid ${theme.palette.grey[300]}`,
    justifyContent: "space-between",
    alignItems: "center",
}));

const BoxContainer = styled(Box)(({theme})=>({
    position: "absolute",
    marginTop:"4rem",
    top: "30%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "60%",
    backgroundColor: "#FFf",
    borderRadius: theme.shape.borderRadius,
    boxShadow: 10,
    padding: theme.spacing(4),
    
    [theme.breakpoints.down("md")]:{
        top: "30%",
        width: "90%",
        left: "53%",
    }
}))

export default function EmpolyeeForm() {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <div>
            <Button
                onClick={handleOpen}
                variant="outlined"
                endIcon={<AddIcon sx={{ mr: 1, ml: 0, width: "1rem" }} />}
                sx={{ direction: "rtl" }}>
                اضافه کردن
            </Button>
            <Modal
                aria-labelledby="spring-modal-title"
                aria-describedby="spring-modal-description"
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}>
                <Fade in={open}>
                    <BoxContainer>
                        <HeaderContainer container>
                            <Grid item>
                                <TypographyTitle
                                    id="spring-modal-title"
                                    variant="h5"
                                    component="h2">
                                    فرم کارمندان
                                </TypographyTitle>
                            </Grid>
                            <Grid item>
                                <IconButton
                                    aria-label="delete"
                                    onClick={handleClose}>
                                    <CancelPresentationRoundedIcon
                                        sx={{
                                            color: "error.light",
                                            fontSize: "2rem",
                                        }}
                                    />
                                </IconButton>
                            </Grid>
                        </HeaderContainer>

                        <Formik
                            initialValues={{
                                ...INITIAL_FORM_STATE,
                            }}
                            validationSchema={FORM_VALIDATION}
                            onSubmit={(values) => {
                                console.log(JSON.stringify(values, null, 2));
                            }}>
                            <Form>
                                <Grid container spacing={5}>
                                    <Grid item xs={6}>
                                        <TextField
                                            name="fullName"
                                            label="نام و نام خانوادگی"
                                        />
                                        <TextField name="email" label="ایمیل" />
                                        <TextField
                                            name="moblieNumber"
                                            label="شماره موبایل"
                                        />
                                        <Select
                                            name="city"
                                            label="شهر"
                                            options={city}
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                         <Radio name="gender" legend="جنسیت" />
                                        <Select
                                            name="department"
                                            label="واحد سازمانی"
                                            options={departmentField}
                                            sx={{ mt: 0.8 }}
                                        />
                                        <DatePicker name="hireDate" />
                                         <Checkbox
                                            name="termsOfService"
                                            legend="قوانین را قبول دارید؟"
                                            label="بله"
                                        />
                                        <Stack direction="row" mt={3}>
                                            <ButtonWrapper>
                                                ارسال اطاعات
                                            </ButtonWrapper>
                                            <Button
                                                variant="contained"
                                                color="error"
                                                sx={{ mr: 2 }}>
                                                انصراف
                                            </Button>
                                        </Stack>
                                    </Grid>
                                </Grid>
                            </Form>
                        </Formik>

                        <Grid container>
                            <Grid item xs={6}></Grid>
                            <Grid item xs={6}></Grid>
                        </Grid>
                    </BoxContainer>
                </Fade>
            </Modal>
        </div>
    );
}
