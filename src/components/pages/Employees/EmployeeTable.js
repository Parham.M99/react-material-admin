import {
    DataGrid,
    GridRowsProp,
    GridColDef,
    GridActionsCellItem,
    GridToolbar,
} from "@mui/x-data-grid";
import { styled } from "@mui/material/styles";
import { useCallback, useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import SecurityIcon from "@mui/icons-material/Security";
import FileCopyIcon from "@mui/icons-material/FileCopy";
import { Grid, useMediaQuery } from "@mui/material";
import { useTheme } from '@mui/material/styles';

function getFullName(params) {
    return `${params.getValue(params.id, "firstName") || ""} ${
        params.getValue(params.id, "lastName") || ""
    }`;
}

function EmployeeTable() {
    const initialRows = [
        { id: 1, lastName: "حسینی", firstName: "محمدی", isAdmin: false },
        { id: 2, lastName: "احمدی", firstName: "رضایی", isAdmin: true },
        { id: 3, lastName: "کریمی", firstName: "موسوی", isAdmin: false },
        { id: 4, lastName: "جعفری", firstName: "اصادقی", isAdmin: false },
        { id: 5, lastName: "حیدری", firstName: "حیدری", isAdmin: false },
    ];

    const [rows, setRows] = useState(initialRows);

    const deleteUser = useCallback(
        (id) => () => {
            setTimeout(() => {
                setRows((prevRows) => prevRows.filter((row) => row.id !== id));
            });
        },
        []
    );

    const toggleAdmin = useCallback(
        (id) => () => {
            setRows((prevRows) =>
                prevRows.map((row) =>
                    row.id === id ? { ...row, isAdmin: !row.isAdmin } : row
                )
            );
        },
        []
    );

    const duplicateUser = useCallback(
        (id) => () => {
            setRows((prevRows) => {
                const rowToDuplicate = prevRows.find((row) => row.id === id);
                return [...prevRows, { ...rowToDuplicate, id: Date.now() }];
            });
        },
        []
    );

    const columns = [
        {
            field: "firstName",
            headerName: "نام",
            width: 130,
            headerClassName: "super-app-theme--header",
            cellClassName: "super-app-theme--cell",
            align: "right",
        },
        {
            field: "lastName",
            headerName: "نام خانوادگی",
            width: 130,
            headerClassName: "super-app-theme--header",
            align: "right",
        },
        {
            field: "fullName",
            headerName: "نام و نام خانوادگی",
            width: 160,
            valueGetter: getFullName,
            headerClassName: "super-app-theme--header",
            align: "right",
        },
        {
            field: "isAdmin",
            headerName: "ادمین",
            type: "boolean",
            headerAlign: "left",
            width: 100,
            headerClassName: "super-app-theme--header",
            align: "left",
        },
        {
            field: "actions",
            type: "actions",
            headerClassName: "super-app-theme--header",
            getActions: (params) => [
                <GridActionsCellItem
                    icon={<DeleteIcon />}
                    label="حذف"
                    onClick={deleteUser(params.id)}
                />,
                <GridActionsCellItem
                    icon={<SecurityIcon />}
                    label="تغییر وضعیت ادمین"
                    onClick={toggleAdmin(params.id)}
                    showInMenu
                />,
                <GridActionsCellItem
                    icon={<FileCopyIcon />}
                    label="تکرار کارمند"
                    onClick={duplicateUser(params.id)}
                    showInMenu
                />,
            ],
        },
    ];

    const DataGridTotal = styled(DataGrid)(({ theme }) => ({
        direction: "rtl",
        ".super-app-theme--header": {
            color: "#1c2971",
            backgroundColor: "#e1e4f3",
        },
        ".super-app-theme--cell": {
            direction: "rtl",
        },
        ".super-app-theme--true": {
            "&:hover": {
                backgroundColor: "#ACA9BB",
            },
        },
        [theme.breakpoints.down("sm")]:{
            disableColumnSelector:"true"
        }
    }));
    const theme = useTheme();

    const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));

    const DataGridContainer = styled("div")(({ theme }) => ({
        height: "350px",
        width: "100%",
        marginTop: 10,
    }));

    const gridProps = {
        disableColumnSelector: isSmallScreen ? true: false
    }

    return (
        <Grid container>
            <DataGridContainer>
                <DataGridTotal
                    columns={columns}
                    rows={rows}
                    getRowClassName={(params) =>
                        `super-app-theme--${params.getValue(
                            params.id,
                            "isAdmin"
                        )}`
                    }
                    components={{
                        Toolbar: GridToolbar,
                    }}
                   {...gridProps}
                />
            </DataGridContainer>
        </Grid>
    );
}

export default EmployeeTable;
