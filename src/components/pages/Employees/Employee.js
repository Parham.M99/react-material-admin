import { Button, Grid, IconButton, Paper, TextField } from "@mui/material";
import { styled } from "@mui/material/styles";
import { Box } from "@mui/system";
import SearchIcon from "@mui/icons-material/Search";
import EmployeeTable from "./EmployeeTable";
import EmpolyeeForm from "./EmpolyeeForm";
import { useState } from "react";

const BoxContainer = styled(Box)(({ theme }) => ({
    backgroundColor: "#F4F5FD",
    height: "72%",
}));

const PaperContainer = styled(Paper)(({ theme }) => ({
    backgroundColor: "##F4F5FD",
    width: "93%",
    marginTop: theme.spacing(1),
    marginLeft: "auto",
    marginRight: "auto",
    padding: theme.spacing(2),
}));

const InputContainer = styled("div")(({ theme }) => ({
    display: "flex",
    alignItems: "flex-end",
    marginBottom: theme.spacing(1.5),
    marginLeft: theme.spacing(1.5),
}));

const TextFieldItem = styled(TextField)(({ theme }) => ({
    "& label": {
        transformOrigin: "top right",
        right: 0,
        left: "auto",
    },
}));

function Employee() {

    // const [showModal, setShowModal] = useState(false);

    // const handleModalOpen = () => {
    //     setShowModal(true);
    // };

    return (
        <>
        <BoxContainer>
            <Box sx={{ visibility: "hidden" }}>.</Box>
            <PaperContainer elevation={3}>
                <Grid container justifyContent="center" alignItems="center">
                    <Grid item md={10}>
                        <InputContainer>
                            <SearchIcon
                                sx={{
                                    color: "action.active",
                                    mr: 1,
                                    ml: 0.5,
                                    my: 0.5,
                                }}
                            />
                            <TextFieldItem
                                id="standard-basic"
                                label="جستجو کارمندان"
                                variant="standard"
                                sx={{ width: "100%" }}
                            />
                        </InputContainer>
                    </Grid>
                    <Grid item md={2}>
                        {<EmpolyeeForm />}
                    </Grid>
                </Grid>
                <EmployeeTable />
            </PaperContainer>
        </BoxContainer>
        </>
    );
}

export default Employee;
